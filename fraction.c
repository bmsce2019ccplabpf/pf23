#include<stdio.h>
void input( int *n1, int *d1, int *n2, int *d2)
	{
		printf("Enter the numerator and denominator of the first fraction\n");
		scanf("%d%d",n1,d1);
		printf("Enter the numerator and denominator of the second fraction\n");
		scanf("%d%d",n2,d2);
	}
void sum(int n1,int d1, int n2, int d2, int *numer,int *denom)
	{
		int i;              
		*numer=((d2*n1)+(d1*n2));
		*denom=d1*d2;
		if (*numer<*denom)
			{
				for(i=*numer;i>=2;i++)
					{
						if (*numer%i==0 && *denom%i==0)
							{	
								*numer=*numer/i;
								*denom=*denom/i;
							}
					}
			 }
			else if (*denom<*numer)
			 {
					for(i=*denom;i>=2;i++)
					 {
							if (*numer%i==0 && *denom%i==0)
								{
								 *numer=*numer/i;
									*denom=*denom/i;
								}
						}
				}
			else
				{
					*numer=1;
					*denom=1;
				}
	}
void output(int numer,int denom)
{
		printf("The sum of the fraction is %d/%d\n",numer,denom);			
}
int main()
	{
			int n1,n2,d1,d2,numer,denom;
			input(&n1,&d1,&n2,&d2);
			sum(n1,d1,n2,d2,&numer,&denom);
			output(numer,denom);
return 0;
  }
