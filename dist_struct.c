#include<stdio.h>
#include<math.h>
void input(float *x1,float *y1, float *x2, float *y2)
{
    printf("Enter the co ordinates of first point\n");
    scanf("%f%f",x1,y1);
    printf("Enter the co ordinates of second point\n");
    scanf("%f%f",x2,y2);
}
float compute(float x1,float y1, float x2, float y2)
{
    float dist=sqrt(pow((x1-x2),2)+pow((y1-y2),2));
    return dist;
}
void output(float dist)
{
    printf("The distance between the two points is %f\n",dist);
}
int main()
{
    float x1,y1,x2,y2,dist;
    struct point
    {
        float x,y;
    };
    struct point p1;
    struct point p2;
    input(&p1.x,&p1.y,&p2.x,&p2.y);
    x1=p1.x;
    x2=p2.x;
    y1=p1.y;
    y2=p2.y;
    dist=compute(x1,y1,x2,y2);
    output(dist);
    return 0;
}