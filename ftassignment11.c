#include<stdio.h>
int main()
{
    int x,y,sol,ch;
    char a;
    do
    {
        printf("Enter 2 nos. ");
        scanf("%d%d",&x,&y);
        printf("\n\nMENU");
        printf("\n1. Add");
        printf("\n2. Subtract");
        printf("\n3. Multiply");
        printf("\n4. Divide");
        printf("\n\nEnter your choice ");
        scanf("%d",&ch);
        switch(ch)
        {
            case 1:
                sol=x+y;
                printf("Sum is %d",sol);
                break;
                
            case 2:
                sol=x-y;
                printf("Difference is %d",sol);
                break;
                
            case 3:
                sol=x*y;
                printf("Product is %d",sol);
                break;
                
            case 4:
                sol=x/y;
                printf("Quotient is %d",sol);
                break;
                
            default: printf("\nInvalid input");
        }
        
        printf("\nDo you want to continue?(y/n) ");
        scanf("%c",&a);
    }
    
    while(a=='y');
    return 0;
}