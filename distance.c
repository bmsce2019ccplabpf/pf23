#include<stdio.h>
#include<math.h>
void input(float *x1,float *x2,float *y1,float *y2)
{
  printf("Enter the co ordinate x1\n ");
	scanf("%f",x1);
	printf("Enter the co ordinate y1\n ");
	scanf("%f",y1);
	printf("Enter the co ordinate x2\n ");
	scanf("%f",x2);
	printf("Enter the co ordinate y2\n ");
	scanf("%f",y2);
}
float distance(int x1,int x2,int y1,int y2)
{
	float dist;
	dist=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return dist;
}
void output(float dist)
{
	printf("The distance between the two points is %f\n",dist);
}
int main()
{
	float x1,x2,y1,y2;
	float dist;
	input(&x1,&x2,&y1,&y2);
	dist=distance(x1,x2,y1,y2);
	output(dist);
	return 0;
}
